﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Virtual_Store.Models
{
    public class ProductItem
    {
        public int Id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }

        public string image { get; set; }
    }
}