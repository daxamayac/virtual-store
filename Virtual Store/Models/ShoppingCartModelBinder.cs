﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Virtual_Store.Models
{
    public class ShoppingCartModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            ShoppingCart cc = (ShoppingCart)controllerContext
                               .HttpContext
                               .Session["shoppingCartSession"];
            if (cc == null)
            {
                cc = new ShoppingCart();
                controllerContext.HttpContext
                                .Session["shoppingCartSession"] = cc;
            }
            return cc;
        }
    }
}