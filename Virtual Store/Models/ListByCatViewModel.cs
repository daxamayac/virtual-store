﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Virtual_Store.Models
{
    public class ListByCatViewModel
    {
        public string SelectedCategory { get; set; }
        public List<SelectListItem> Categories { get; set; } = new List<SelectListItem>();
        public List<Product> SearchResults { get; set; } = new List<Product>();
    }
}
