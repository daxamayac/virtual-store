﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Virtual_Store.Startup))]
namespace Virtual_Store
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
