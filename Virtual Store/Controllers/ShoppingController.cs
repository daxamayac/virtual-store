﻿using System;
using System.Linq;
using System.Web.Mvc;
using Virtual_Store.Models;

namespace Virtual_Store.Controllers
{
    public class ShoppingController : Controller
    {
        private VirtualStoreDBEntities db = new VirtualStoreDBEntities();
        private double total = 0;

        // GET: Shopping
        public ActionResult Index()
        {
            ViewBag.categories = db.Categories.ToList();
            ViewBag.products = db.Products.ToList();
            return View();
        }


        public ActionResult Category(int id)
        {
            var categories = db.Categories.ToList();
            var categoryEnable = db.Categories.Find(id);
            var products = categoryEnable.Products;
            ViewBag.categoryEnable = categoryEnable.name;
            ViewBag.categories = categories;
            ViewBag.products = products;

            return View("Index");
        }


        public ActionResult Add(int id, ShoppingCart sc)
        {
            Product prod = db.Products.Find(id);

            ProductItem pi = new ProductItem();
            pi.Id = prod.Id;
            pi.image = prod.image;
            pi.name = prod.name;
            pi.quantity = 1;
            pi.price = prod.price;

            if (sc.Find(item => item.Id == pi.Id) != null) {
                sc.Find(item => item.Id == pi.Id).quantity += 1;
            }
            else
                sc.Add(pi);


            ViewBag.total = sc.Sum(item => item.price * item.quantity);

            return View("Cart", sc);
        }

        public ActionResult Remove(int id, ShoppingCart sc)
        {

            if (sc.Find(item => item.Id == id) != null)
            {
                sc.Remove(sc.Find(item => item.Id == id));
            }

            ViewBag.total = sc.Sum(item => item.price * item.quantity);

            return View("Cart", sc);
        }

        public ActionResult Cart(ShoppingCart sc)
        {
            ViewBag.total = sc.Sum(item => item.price * item.quantity);

            return View("Cart", sc);
        }

        [Authorize]
        public ActionResult Payment(ShoppingCart sc)
        {
            ViewBag.total = sc.Sum(item => item.price * item.quantity);
            ViewBag.cart = sc;

            return View("Payment");
        }
        [Authorize]
        public ActionResult Pay(ShoppingCart sc)
        {

            Order o = new Order();

            o.date = DateTime.UtcNow;
            o.referenceUser = User.Identity.Name;
            o.status = "Enviado";
            o.shippingAddress = "Direccion de envio comprador";
            o.name = User.Identity.Name;
            db.Orders.Add(o);
            db.SaveChanges();

            double total = 0;

            foreach (ProductItem pi in sc.ToList()) {

                OrderDetail od = new OrderDetail();
                Product p = db.Products.Find(pi.Id);
                od.quantity = pi.quantity;
                od.Product = p;
                od.price = pi.price;
                od.Order = o;
                db.OrderDetails.Add(od);

                
                p.stock = p.stock - pi.quantity;
                total += pi.quantity * pi.price;

                db.SaveChanges();
            }

            o.total = total;
            db.SaveChanges();

            Session.Clear();

            ViewBag.orders = db.Orders.Where(user => user.referenceUser == User.Identity.Name).OrderByDescending(or => or.date).ToList();

            return View("Orders");
        }
        [Authorize]
        public ActionResult Orders()
        {
            ViewBag.orders = db.Orders.Where(user => user.referenceUser == User.Identity.Name).OrderByDescending(or => or.date).ToList();

            return View("Orders");
        }
    }
}