﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Virtual_Store.Models;

namespace Virtual_Store.Controllers
{
    public class HomeController : Controller
    {
        private VirtualStoreDBEntities db = new VirtualStoreDBEntities();
        public ActionResult Index()
        {

            var products = db.Products;

            ViewBag.Products = products.Where(p => p.price > 100 && p.stock>5)
                .OrderBy(p=>p.price)
                .Take(5)
                .ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Virtual Store.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Más información en.";
            ViewBag.GitRepo = "https://gitlab.com/daxamayac/virtual-store.git";
            return View();
        }
    }
}